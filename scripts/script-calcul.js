$(document).ready(function () {		
	
	$('#add').click(function () {
        var rotation = '<div class="row delete" id="rotation"><div class="block_bonus"><div class="dmg_bonus_pct"><span>Урон + <input type="text" id="confirm_dmg_bonus_pct" [dynamicWidth]="{ minWidth: 1, maxWidth: 200 }" readonly> %</span></div><div class="vaporize_output"><span>Пар</span></div></div><div id="hide_bonus"><div class="enter_bonus" id="enter_bonus"><h3 class="rot-title bonus-title">Бонусы</h3><div class="table_enter"><table><tr><td><label for="atk-hp-def_pct">+ATK/HP/DEF, %</label></td><td><input type="text" value="0%" id="atk-hp-def_pct" name="atk-hp-def_pct"></td></tr><tr><td><label for="atk-hp-def">+ATK/HP/DEF</label></td><td><input type="text" value="0" id="atk-hp-def" name="atk-hp-def"></td>' +
            '</tr><tr><td><label for="em">+Мастерство стихий</label></td><td><input type="text" value="0" id="em" name="em" oninput="confirm_em.value = em.value"></td></tr><tr><td><label for="react_bonus_pct">+Бонус реакции, %</label></td><td><input type="text" value="0%" id="react_bonus_pct" name="react_bonus_pct"></td></tr><tr><td><label for="crit_rate_pct">+Крит. шанс, %</label></td><td><input type="text" value="0%" id="crit_rate_pct" name="crit_rate_pct"></td></tr><tr><td><label for="crit_dmg_pct">+Крит. урон, %</label></td><td><input type="text" value="0%" id="crit_dmg_pct" name="crit_dmg_pct"></td></tr><tr><td><label for="dmg_bonus_pct">+Бонус урона, %</label></td><td><input type="text" value="0%" id="dmg_bonus_pct" name="dmg_bonus_pct" oninput="confirm_dmg_bonus_pct.value = dmg_bonus_pct.value"></td></tr>' +
            '<tr><td><label for="res_red_pct">+Срез RES, %</label></td><td><input type="text" value="0%" id="res_red_pct" name="res_red_pct"></td></tr><tr><td><label for="def_red_pct">+Срез DEF, %</label></td><td><input type="text" value="0%" id="def_red_pct" name="def_red_pct"></td></tr><tr><td><label for="def_ignore_pct">+Игнор. DEF, %</label></td><td><input type="text" value="0%" id="def_ignore_pct" name="def_ignore_pct"></td></tr><tr><td><label for="multiplicator">xМультипликатор</label>' +
            '</td><td><input type="text" value="1" id="multiplicator" name="multiplicator"></td></tr></table><div class="react_block"><a class="without_react_active" id="without_react">Нет</a><a class="react_mini_block" id="vaporize">Пар</a><a class="react_mini_block" id="melt">Таяние</a></div></div></div></div><div class="settings block_settings" id="settings"><a href="#"><img src="/img/settings.png" alt="settings" class="settings"></a></div><div><select name="rotations" id="rotations"><option value="n1a" selected>N1a</option><option value="n1b">N1b</option><option value="n2">N2</option><option value="n3">N2</option></select></div><div class="minus block_minus remove"><a href="#"><img src="/img/minus.svg" alt="minus" class="minus"></a></div></div>'
        
        $("#list").append(rotation);
	});
			
	$("body").on("click", ".remove", function(){
		$(this).parents(".delete").remove();
    });
    
    $("body").on("click", "#settings", function(){
        $(this).parents("#rotation").children('#hide_bonus').toggle();
	});
	
	// $('#settings').click(function () {	
	// 	$('#hide_bonus').toggle(300);
	// })

	$("#multiplicator, #dmg_bonus_pct, #atk-hp-def_pct, #react_bonus_pct, #crit_rate_pct, #crit_dmg_pct, #res_red_pct, #def_red_pct, #def_ignore_pct, #atk-hp-def, #em").keypress(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8) { }
        else {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault(); 
            }   
        }
    });

    $("#dmg_bonus_pct, #atk-hp-def_pct, #react_bonus_pct, #crit_rate_pct, #crit_dmg_pct, #res_red_pct, #def_red_pct, #def_ignore_pct").change(function() {
        if ($(this).val()[0] == 0) { 
            $(this).val($(this).val().replace(/^0+%/, ''));
        }
    });

    $("#atk-hp-def, #em").change(function() {
        if ($(this).val()[0] == 0) { 
            $(this).val($(this).val().replace(/^0+/, ''));
        }
    });

    $("#dmg_bonus_pct").change(function() {
        if ($('#confirm_dmg_bonus_pct').val()[0] == 0) { 
            $('#confirm_dmg_bonus_pct').val($('#confirm_dmg_bonus_pct').val().replace(/^0+%/, ''));
        }
    });

    $('#dmg_bonus_pct').change(function () {
        let dmg_bonus_pct = $('#dmg_bonus_pct').val()
        if (dmg_bonus_pct > 0) {
            $('.dmg_bonus_pct').show();
            $('#dmg_bonus_pct').addClass("input_green");
        } else {
            $('#dmg_bonus_pct').removeClass("input_green");
            $('.dmg_bonus_pct').hide();
        }
    });

    $("#dmg_bonus_pct, #atk-hp-def_pct, #react_bonus_pct, #crit_rate_pct, #crit_dmg_pct, #res_red_pct, #def_red_pct, #def_ignore_pct").on("focus", function () {
        var hours = $.trim($(this).val());
        if(!hours){
            $(this).val(0 + '%');
        }
    });
    
    $("#atk-hp-def, #em").on("focus", function () {
        var hours = $.trim($(this).val());
        if(!hours){
            $(this).val(0);
        }
    });

    $("#multiplicator").on("focus", function () {
        var hours = $.trim($(this).val());
        if(!hours){
            $(this).val(1);
        }
    });

    $('#dmg_bonus_pct').change(function () {
        let value = $("#dmg_bonus_pct").val();
        let newWidth = value.length;
        $("#confirm_dmg_bonus_pct").width(newWidth + 'ch');
    });

    $('#dmg_bonus_pct, #atk-hp-def_pct, #react_bonus_pct, #crit_rate_pct, #crit_dmg_pct, #res_red_pct, #def_red_pct, #def_ignore_pct').change(function () {
        let input_lenght = $(this).val().length;
        if ($(this).val()[input_lenght-1] !== '%') {
            $(this).val($(this).val() + '%');
        };
    });

    $("body").on("click", "#vaporize", function () {
        $(this).toggleClass("react_mini_block vaporize_active");
        $(this).parents('.react_block').parents('.table_enter').parents('#enter_bonus').
            parents('#hide_bonus').parents('#rotation').children('.block_bonus').children('.vaporize_output').toggle();
    });

    $('#vaporize').on("click", function () {
        if ($("#vaporize").hasClass("vaporize_active")) {
            $('#without_react').toggleClass("without_react_active react_mini_block");
        } else {
            $('#without_react').toggleClass("react_mini_block without_react_active");
        }
    });

    $('#without_react').on("click", function () {
        if ($("#without_react").hasClass("without_react_active")) { }
        else {
            $('#without_react').toggleClass("react_mini_block without_react_active");
            $("#vaporize").toggleClass("vaporize_active react_mini_block")
            $('.vaporize_output').toggle();
        }
    });
});

